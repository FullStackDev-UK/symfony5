<?php

namespace App\Tests\Functional;

use App\Entity\Product;
use App\Tests\BaseTest;

class DatabaseTest extends BaseTest
{
    public function test_it_can_create_a_product_and_save_it_to_the_database(): void
    {
        $product = new Product();
        $product->setName("PlayStation");
        $product->setDescription("This is a great console.");
        $product->setPrice(400);

        $this->entityManager->persist( $product );
        $this->entityManager->flush();
        
        $productRepository = $this->entityManager->getRepository( Product::class );

        $product = $productRepository->findBy(['name' => 'PlayStation']);

        $productDescription = $product[0]->getDescription();
        
        $this->assertEquals( "This is a great console.", $productDescription );
    }

    public function test_it_can_search_by_name(): void
    {
        $product = new Product();
        $product->setName("PlayStation");
        $product->setDescription("This is a great console.");
        $product->setPrice(400);

        $this->entityManager->persist( $product );
        $this->entityManager->flush();
        
        $product = $this->entityManager->getRepository( Product::class )->findOneBy( ['name' => 'PlayStation'] );

        $this->assertSame( 400, $product->getPrice() );
    }
}