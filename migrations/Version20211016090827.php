<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211016090827 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `product` ADD `name` VARCHAR(255) NULL DEFAULT NULL AFTER `id`;');
        $this->addSql('ALTER TABLE `product` ADD `description` VARCHAR(255) NULL DEFAULT NULL AFTER `name`;');
        $this->addSql('ALTER TABLE `product` ADD `price` INTEGER(25) NULL DEFAULT NULL AFTER `description`;');
        $this->addSql('ALTER TABLE `product` ADD `image` VARCHAR(255) NULL DEFAULT NULL AFTER `price`;');
        $this->addSql('INSERT INTO `product` (`id`, `name`, `description`, `price`, `image`) VALUES (NULL, "Nintendo", "Games console", 200), (NULL, "PlayStation", "By Sony", 300, NULL);');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE product');
    }
}
