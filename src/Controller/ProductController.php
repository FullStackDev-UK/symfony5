<?php

namespace App\Controller;

use App\Entity\Product;
use Psr\Log\LoggerInterface;
use App\Repository\ProductRepository;
use App\Services\UploadManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    public function list( LoggerInterface $logger, ProductRepository $productRepository, EntityManagerInterface $em ): Response
    {
        $products = $productRepository->findAll();
        // $products = $em->getRepository( Product::class )->findAll();

        $logger->info( "Products listed" );

        return $this->render( 'page/products.html.twig', [
            'page_heading' => 'Products',
            'products' => $products
        ]);
    }

    public function add( Request $request, EntityManagerInterface $em, UploadManager $uploadManager ): Response
    {
        $request = Request::createFromGlobals();
        $productname = $request->request->get('productname', 'default category');
        $productdescription = $request->request->get('productdescription', 'default category');
        $productprice = $request->request->get('productprice', 'default category');
        
        $product = new Product();
        $product->setName($productname);
        $product->setDescription($productdescription);
        $product->setPrice($productprice);

        $uploadedFile = $request->files->get('productimage');
        if ( $uploadedFile ) {
            $newFilename = $uploadManager->uploadImage( $uploadedFile );
            $product->setImage( $newFilename );
        }

        $em->persist($product);
        $em->flush();

        return $this->redirectToRoute('products');
    }

    public function edit( Request $request, ProductRepository $productRepository )
    {
        $id = $request->attributes->all()['id'];
        $product = $productRepository->find( $id );

        $productId = $id;
        $productName = $product?->getName() ?: 'Unset';
        $productDescription = $product?->getDescription() ?: 'Unset';
        $productPrice = $product?->getPrice() ?: 0;
        $productImage = $product?->getImage() ?: null;

        return $this->render('page/editproduct.html.twig', [
            'page_heading' => 'Edit product',
            'productId' => $productId,
            'productName' => $productName,
            'productDescription' => $productDescription,
            'productPrice' => $productPrice,
            'productImage' => $productImage
        ]);
    }
    
    public function save( Request $request, EntityManagerInterface $em, UploadManager $uploadManager )
    {
        $request = Request::createFromGlobals();
        $id = $request->request->get('productid', null);
        $productname = $request->request->get('productname', 'Product name unset');
        $productdescription = $request->request->get('productdescription', 'Product description unset');
        $productprice = $request->request->get('productprice', 'Product price unset');

        $product = ( $id ) ? $em->getRepository( Product::class )->find( $id ) : new Product();

        $product->setName($productname);
        $product->setDescription($productdescription);
        $product->setPrice($productprice);

        $uploadedFile = $request->files->get('productimage');
        if ( $uploadedFile ) {
            $newFilename = $uploadManager->uploadImage( $uploadedFile );
            $product->setImage( $newFilename );
        }

        $em->persist($product);
        $em->flush();

        return $this->redirectToRoute('products');
    }

    public function delete( Request $request, ProductRepository $productRepository, EntityManagerInterface $em )
    {
        $id = $request->attributes->all()['id'];
        $product = $productRepository->find( $id );

        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('products');
    }
}