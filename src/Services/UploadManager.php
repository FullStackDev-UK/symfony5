<?php

namespace App\Services;

use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadManager
{
    /**
     * @var string
     */
    private $uploadsPath;
    
    private $slugger;

    /**
     * @param string $uploadsPath    Is set in services.yaml
     */
    public function __construct( string $uploadsPath, SluggerInterface $slugger )
    {
        $this->uploadsPath = $uploadsPath;
        $this->slugger = $slugger;
    }

    public function uploadImage( UploadedFile $uploadedFile ): string
    {
        $destination = $this->uploadsPath;

        /** @var UploadedFile $uploadedFile */
        $originalFilename = pathinfo( $uploadedFile->getClientOriginalName(), PATHINFO_FILENAME );

        $newFilename = strtolower( $this->slugger->slug( $originalFilename ) ).'-'.uniqid().'.'.$uploadedFile->guessExtension();

        $uploadedFile->move( $destination, $newFilename );

        return $newFilename;
    }
}


