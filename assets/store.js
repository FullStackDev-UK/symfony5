import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        username: 'John Doe'
    },
    actions: {
        changeProperty ({ commit }) {
            commit('changeProperty');
        }
    },
    mutations: {
        changeProperty(variable) {
            this.state.username = variable;
        }
    },
    getters: {
        username: state => state == null ? null : state.username
    }
});
export default store;
