/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

import store from './store';
Vue.config.productionTip = false;

import BaseComponent from './components/BaseComponent';
Vue.component('BaseComponent', BaseComponent );

let data = {
    message: "Welcome",
    username: store.state.username,
    fruits: ['apple', 'banana'],
    fruitEmoji: { 'apple': '🍎', 'banana': '🍌' }
};

const Vueapp = new Vue({
    store,
    data,
    el: '#vueapp',
    delimiter: ['{}'],
    render: h => h(Vueapp)
});